package com.spring.finalproject.repository;

import com.spring.finalproject.entity.FeedbackEntity;
import com.spring.finalproject.entity.ProductSpecificationEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface FeedbackRepository extends CrudRepository<FeedbackEntity, Long> {

    @Transactional
    @Modifying
    @Query(value = "SELECT * \n" +
            "FROM feedback\n" +
            "where proid = ?1", nativeQuery = true)
    List<FeedbackEntity> getFeedbackEntityById(long id);
}
