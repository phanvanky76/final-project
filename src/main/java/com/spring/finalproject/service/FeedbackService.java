package com.spring.finalproject.service;

import com.spring.finalproject.entity.FeedbackEntity;
import com.spring.finalproject.entity.ProductEntity;
import com.spring.finalproject.repository.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeedbackService {
    @Autowired
    FeedbackRepository feedbackRepository;


    public void submitComment(FeedbackEntity feedback, String body) {
        String[] commentArr = body.split(",");
        if (commentArr.length >= 3) {
            FeedbackEntity comments = new FeedbackEntity(commentArr[0], commentArr[1], Integer.valueOf(commentArr[2]));
            feedback.setUsername(commentArr[0]);
            feedback.setContent(commentArr[1]);
            feedback.setRates(Integer.valueOf(commentArr[2]));
            ProductEntity productEntity = new ProductEntity();
            productEntity.setId(Integer.parseInt(commentArr[3]));
            feedback.setProductFeedback(productEntity);

            feedbackRepository.save(feedback);

        }
    }
}
