package com.spring.finalproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "shipping")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ShippingFeesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shipId")
    private long shipId;

    @Column(name = "region")
    private String region;

    @Column(name = "price")
    private double price;

    @OneToMany(mappedBy = "shippingFees")
    private List<CartEntity> cartList;

    @ManyToOne
    @JoinColumn(name = "delete_id")
    private DeleteLogarithmEntity shippingDelete;
}
