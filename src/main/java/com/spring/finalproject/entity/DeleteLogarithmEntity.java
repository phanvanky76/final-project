package com.spring.finalproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "delete_logaritm")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeleteLogarithmEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "deleteStatus")
    private String deleteStatus;

    @OneToMany(mappedBy = "shippingDelete")
    List<ShippingFeesEntity> shippingFeesEntities;

    @OneToMany(mappedBy = "categoryDelete")
    List<CategoryEntity> categoryEntities;

    @OneToMany(mappedBy = "productDelete")
    List<ProductEntity> productEntities;
}


