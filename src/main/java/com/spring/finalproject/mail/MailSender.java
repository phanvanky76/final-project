package com.spring.finalproject.mail;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

public class MailSender {


    private static String SENDER_EMAIL = "conos.team@gmail.com";
    private static String SENDER_EMAIL_PWD = "lqxcizskmcsuysxl"; //axoninsight123

    public static void send(String to, String subject, String qrCodePath, String htmlText)
            throws MessagingException, UnsupportedEncodingException {
        //1 - Get an email session
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtps");
        props.put("mail.smtps.host", "smtp.gmail.com");
        props.put("mail.smtps.port", 465);
        props.put("mail.smtps.auth", "true");
        props.put("mail.smtps.quitwait", "false");
        Session session = Session.getDefaultInstance(props);
        session.setDebug(true);

        //2 - Create a message
        Message message = new MimeMessage(session);
        message.setSubject(subject);

        // This mail has 2 part, the BODY and the embedded image
        MimeMultipart multipart = new MimeMultipart("related");

        // first part (the html)
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(htmlText, "text/html");
        // add it
        multipart.addBodyPart(messageBodyPart);

        // second part (the image)
        messageBodyPart = new MimeBodyPart();
        DataSource fds = new FileDataSource(qrCodePath);

        messageBodyPart.setDataHandler(new DataHandler(fds));
        messageBodyPart.setHeader("Content-ID", "<image>");

        // add image to the multipart
        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart);

        // 3 - address the message
        Address fromAddress = new InternetAddress(SENDER_EMAIL, "TK Apple Store");
        Address toAddress = new InternetAddress(to);
        message.setFrom(fromAddress);
        message.setRecipient(Message.RecipientType.TO, toAddress);

        // 4 - send the message
        Transport transport = session.getTransport();
        transport.connect(SENDER_EMAIL, SENDER_EMAIL_PWD);
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }
}