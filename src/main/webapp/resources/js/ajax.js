jQuery(document).ready(function($) {
    $("#submitCommentForm").submit(function(event) {
        // Prevent the form from submitting via the browser.
        event.preventDefault();
        submitComment();
    });

});

function submitComment() {
    let comment = $("#username").val() + "," + $("#content").val() + "," + $("#rating").val()+ "," + $("#productFeedback").val();

    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : "/user/comment/submit",
        data : comment,
        timeout : 100000,
        success : function(data) {
            console.log(data);
            display(data);
        }
    });
}

function display(data) {
    var comment = data.split('||');
    // comment[2]: rating, 1 -> 1 star, 2 = 2 star and so on
    switch(parseInt(comment[2])) {
        case 1:
            var rate = "<span style=\"cursor: default;\">\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "<div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: visible;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>"+
                "<div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: visible;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>"+
                "<div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: visible;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>"+
                "<div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: visible;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>"+
                "  </span>"
            break;
        case 2:
            var rate = "<span style=\"cursor: default;\">\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "<div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: visible;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>"+
                "<div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: visible;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>"+
                "<div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: visible;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>"+
                "  </span>"
            break;
        case 3:
            var rate  = "<span style=\"cursor: default;\">\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "<div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: visible;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>"+
                "<div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: visible;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>"+
                "  </span>"
            break;
        case 4:
            var rate  = "<span style=\"cursor: default;\">\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "<div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: visible;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: 0%;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>"+
                "  </span>"
            break;
        default:
            var rate = "<span style=\"cursor: default;\">\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "    <div class=\"rating-symbol\" style=\"display: inline-block; position: relative;\">\n" +
                "      <div class=\"rating-symbol-background glyphicon glyphicon-star-empty\" style=\"visibility: hidden;\"></div>\n" +
                "      <div class=\"rating-symbol-foreground\"\n" +
                "        style=\"display: inline-block; position: absolute; overflow: hidden; left: 0px; right: 0px; width: auto;\"><span\n" +
                "          class=\"glyphicon glyphicon-star\"></span></div>\n" +
                "    </div>\n" +
                "  </span>\n"
            break;
    }

    var newComment = "<div class='form-group'>"
        + "<label>" +comment[0] + "</label>"
        + "<p>" + rate + "</p>"
        + "<input type='text' class='form-control' placeholder='" + comment[1] + "' readonly>"
        + "</div>";
    $('#commentList').append(newComment);
}