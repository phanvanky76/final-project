<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>Register</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link href="/resources-management/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/resources-management/plugins/iconic/css/material-design-iconic-font.min.css">
    <!-- bootstrap -->
    <link href="/resources-management/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="/resources-management/css/pages/extra_pages.css">
    <!-- favicon -->
    <link rel="shortcut icon" href="/resources-management/img/favicon.ico" />
</head>
<body>
<div class="limiter">
    <div class="container-login100 page-background">
        <div class="wrap-login100">
            <form:form action="${action}" method="post" modelAttribute="account" class="login100-form validate-form">
					<span class="login100-form-logo">
						<i class="zmdi zmdi-flower"></i>
					</span>
                <span class="login100-form-title p-b-34 p-t-27">
						<c:out value="${mess}"/>
					</span>
                <div class="row">
                    <c:if test="${message != null}">
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                ${message}
                        </div>
                    </c:if>
                    <c:if test="${type.equals('update')}">
                        <div class="col-lg-6 p-t-20">
                            <div class="wrap-input100 validate-input" data-validate = "Enter username">
                                <form:input path="fullName" class="input100" type="text" name="username" placeholder="Username" readonly="true"/>
                                <span class="focus-input100" data-placeholder="&#xf207;"></span>
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="wrap-input100 validate-input" data-validate = "Enter email">
                                <form:input path="email" class="input100" type="email" name="email" placeholder="Email" readonly="true"/>
                                <form:hidden path="id" name="id"/>
                                <span class="focus-input100" data-placeholder="&#xf207;"></span>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${type == null}">
                        <div class="col-lg-6 p-t-20">
                            <div class="wrap-input100 validate-input" data-validate = "Enter username">
                                <form:input path="fullName" class="input100" type="text" name="username" placeholder="Username"/>
                                <span class="focus-input100" data-placeholder="&#xf207;"></span>
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="wrap-input100 validate-input" data-validate = "Enter email">
                                <form:input path="email" class="input100" type="email" name="email" placeholder="Email"/>
                                <span class="focus-input100" data-placeholder="&#xf207;"></span>
                            </div>
                        </div>
                    </c:if>


                    <div class="col-lg-6 p-t-20">
                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <form:input path="password" id="pswd1" class="input100" type="password" name="pass" placeholder="Password" />
                            <span class="focus-input100" id="message1" data-placeholder="&#xf191;"></span>
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="wrap-input100 validate-input" data-validate="Enter password again">
                            <input class="input100" id="pswd2" type="password" name="pass2" placeholder="Confirm password" >
                            <span class="focus-input100" id="message2" data-placeholder="&#xf191;"></span>
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="wrap-input100 validate-input" data-validate="Enter Phone Number">
                            <form:input path="phoneNumber" class="input100" type="text" name="phoneNumber" placeholder="Phone Number" pattern = "-?[0-9]*(\.[0-9]+)?"  />
                            <span class="focus-input100" data-placeholder="&#xf191;"></span>
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="wrap-input100 validate-input" data-validate="Enter username">
                            <form:input path="address" class="input100" type="text" name="address" placeholder="Address" />
                            <span class="focus-input100" data-placeholder="&#xf191;"></span>
                        </div>
                    </div>

                </div>
                <div class="contact100-form-checkbox">
                    <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                    <label class="label-checkbox100" for="ckb1">
                        Remember me
                    </label>
                </div>
                <div class="container-login100-form-btn">
                    <c:if test="${type.equals('update')}">
                        <button type="submit" class="login100-form-btn">
                            Save
                        </button>
                    </c:if>
                    <c:if test="${type == null}">
                        <button type="submit" class="login100-form-btn">
                            Sign in
                        </button>
                    </c:if>
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <a href="/" class="login100-form-btn">
                        Home
                    </a>
                </div>
            </form:form>
        </div>
    </div>
</div>
<!-- start js include path -->
<script src="/resources-management/plugins/jquery/jquery.min.js" ></script>
<!-- bootstrap -->
<script src="/resources-management/plugins/bootstrap/js/bootstrap.min.js" ></script>
<script src="/resources-management/js/pages/extra_pages/login.js" ></script>
<script>
    function validateForm() {
        //collect form data in JavaScript variables
        var pw1 = document.getElementById("pswd1").value;
        var pw2 = document.getElementById("pswd2").value;

        //check empty password field
        if(pw1 == "") {
            document.getElementById("message1").innerHTML = "**Fill the password please!";
            return false;
        }

        //check empty confirm password field
        if(pw2 == "") {
            document.getElementById("message2").innerHTML = "**Enter the password please!";
            return false;
        }

        //minimum password length validation
        if(pw1.length < 8) {
            document.getElementById("message1").innerHTML = "**Password length must be atleast 8 characters";
            return false;
        }

        //maximum length of password validation
        if(pw1.length > 32) {
            document.getElementById("message1").innerHTML = "**Password length must not exceed 32 characters";
            return false;
        }

        if(pw1 != pw2) {
            document.getElementById("message2").innerHTML = "**Passwords are not same";
            return false;
        } else {
            alert ("Your password created successfully");
            // document.write("JavaScript form has been submitted successfully");
        }
    }
</script>
<!-- end js include path -->
</body>
</html>